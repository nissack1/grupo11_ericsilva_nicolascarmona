'''Diseña un programa que, dados dos números enteros,muestre por pantalla uno de estos mensajes:
 «El segundo es el cuadrado del primero», «El segundo es menor que el cuadrado del primero»
 o bien «El segundo es mayor que el cuadrado del primero»
 dependiendo de la verificación de la condición correspondiente al significado de cada mensaje '''

 '''
 descripción de las funciones

 def get_number():
 	solicita el ingreso de un numero y lo devuelve

 def compare(x,y):
 	Recibe los numeros a evaluar y aplica criterios imprimiendo el resultado

 '''


def get_number():

	num = float(input(f"Ingrese un número: "))
	return num


def compare(x,y):

	if x == y**2:
		print("el segundo es el cuadrado del primero")
	elif y < x**2:
		print("el segundo es menor que el cuadrado del primero")
	elif y > x**2:
		print("el segundo es mayor que el cuadrado del primero")
	else:
		print("error inesperado")

print("ingrese dos numeros para ser evaluados")

compare(get_number(),get_number())



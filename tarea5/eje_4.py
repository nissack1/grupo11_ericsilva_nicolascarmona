'''Diseñe un programa que lea la edad de dos personas y diga quién es más joven, la
primera o la segunda. Tenga en cuenta que ambas pueden tener la misma edad. En tal
caso, debe desplegar un mensaje por pantalla adecuado.'''


''' Descripción de las funciones
 def get_age(d):
 		Recibe una lista que es convertida a un string para poder transformar
 		la información en fomato fecha utilizando la función strptime del modulo datatime

 		dicha fecha en conjunto con la fecha actual, se realizan calculos aritmeticos para obtener 
 		los años de edad.
 		Se compara el mes y día para comprobar si la fecha de nacimiento esta antes o despues
 		de la fecha actual. Si el mes y día no supera la fecha actual antes al calculo de edad
 		se resta una unidad a la edad.

 		retorna la edad

 def get_date():
 	Esta función solicita el ingreso por teclado la fecha de nacimiento que es almacenada
 	en una lista

 	retorna la lista

 def compare_age(x,y):
 	Se ingresan las fechas de nacimiento de cada sujeto (x,y) y se evalúan tres casos:
 		0) x es menor que y?
 		1) tiene la misma edad?
 		2) y es menor que x?

 	imprime la respyesta asociado cada caso

'''

def get_age(d):
  from datetime import date, datetime
  date = '-'.join(map(str,d))
  date = datetime.strptime(date,"%Y-%m-%d")
  today = date.today()
  age = today.year - date.year
  if ((today.month, today.day) < (date.month, date.day)):
    age = age-1
  return age

def get_date():
  format = ["año", "mes", "dia"]
  data = []
  for i, item in enumerate(format):
    inp = str(input(f"Ingrese fecha de nacimiento, {item}:"))
    data.append(inp)

  return data

def compare_ages(x,y):
	if x < y:
		print(f"El primer sujeto tiene {x} años y es menor que el segundo con {y} años")
	elif x == y:
		print(f"Los sujetos tienen la misma edad con {x} años")
	else:
		print(f"El primer sujeto tiene {y} años y es menor que el primero con {x} años")


print("Este programa compara la edad de dos personas y determina quien es menor")
print("Ingrese las fechas de nacimiento en el siguiente orden:")
print("sujeto *: aaaa/mm/dd")

compare_ages(get_age(get_date()), get_age(get_date()))

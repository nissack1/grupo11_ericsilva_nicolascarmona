import math

r=int(input("Intruduzca el radio del cono: "))
h=int(input("Intruduzca la altura del cono: "))

g=math.sqrt(r*r+h*h)
a_base=round(math.pi*r*r,3)
a_lado=round(math.pi*r*g,3)

print("Area base cono: ",a_base)
print("Area lateral cono: ", a_lado)
print("Area Total del cono: ", round(a_base + a_lado,3))
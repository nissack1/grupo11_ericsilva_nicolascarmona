varA = 'a'  #variable asumida 1
varB = 'v'  #variable asumida 2


res1 = isinstance(varA, str)  #verificacion de string o numero
res2 = isinstance(varB, str)


if res1*res2 == 1 :          #si ambos resultados son TRUE caso string involucrado
  print ("string involucrado")

else:                        #caso en que la variable asumida son numero
  if varA > varB  :            
    print ("mas grande")

  if varA == varB  :
    print ("igual")

  if varA < varB  :
    print ("mas pequeño")
